import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class AbusiveUsers {

    /*
     * Complete the 'processLogs' function below.
     *
     * The function is expected to return a STRING_ARRAY.
     * The function accepts following parameters:
     *  1. STRING_ARRAY logs
     *  2. INTEGER threshold
     */

    public static List<String> processLogs(List<String> logs, int threshold) {
        // System.out.println(logs);
        // step 1 : build the table counter for each user
        Map<Integer, Integer> transactionsByUserId = new HashMap<>();
        for (String log : logs) {
            String[] split = log.split(" ");
            Integer userId1 = Integer.valueOf(split[0]);
            Integer userId2 = Integer.valueOf(split[1]);
            transactionsByUserId.merge(userId1, 1,  Integer::sum);
            if (!userId1.equals(userId2)) {
                transactionsByUserId.merge(userId2, 1, Integer::sum);
//                transactionsByUserId.put(userId1, transactionsByUserId.getOrDefault(userId1, 0) + 1);
//                transactionsByUserId.put(userId2, transactionsByUserId.getOrDefault(userId2, 0) + 1);
            }
        }
        // step 2 : return the userIDs whose counters are above or equal to the threshold
        System.out.println(transactionsByUserId);

        List<String> abusiveUserIds = transactionsByUserId.entrySet().stream()
                .filter(entry -> entry.getValue() >= threshold)
                .map(Map.Entry::getKey)
                .sorted()
                .map(String::valueOf)
                .collect(Collectors.toList());

        return abusiveUserIds;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int logsCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<String> logs = IntStream.range(0, logsCount).mapToObj(i -> {
            try {
                return bufferedReader.readLine();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        })
                .collect(toList());

        int threshold = Integer.parseInt(bufferedReader.readLine().trim());

        List<String> result = processLogs(logs, threshold);

        bufferedWriter.write(
                result.stream()
                        .collect(joining("\n"))
                        + "\n"
        );

        bufferedReader.close();
        bufferedWriter.close();
    }
}

import java.util.*;
import java.util.stream.Collectors;

public class HarryPotter {

    static double getPrice(List<Integer> booksBasket) {
        double price = 0.0;

        List<Set<Integer>> bestBundles = findBestBundles(booksBasket);

        for (Set<Integer> bundle : bestBundles) {
            double bundlePrice = 8.0 * bundle.size() * getDiscount(bundle.size());
            price += bundlePrice;
        }

        return price;
    }

    private static List<Set<Integer>> findBestBundles(List<Integer> books) {
        List<Set<Integer>> bundles = bundleAllBooks(books);

        balanceBundles(bundles);

        return bundles;
    }

    private static List<Set<Integer>> bundleAllBooks(List<Integer> books) {
        List<Set<Integer>> bundles = new ArrayList<>();
        while (!books.isEmpty()) {
            Set<Integer> bundle = new HashSet<>(books);
            bundles.add(new HashSet<>(bundle));
            removeBundleFromBasket(books, bundle);
        }
        return bundles;
    }

    private static void removeBundleFromBasket(List<Integer> books, Set<Integer> uniqueBooks) {
        for (Iterator<Integer> iterator = books.iterator(); iterator.hasNext(); ) {
            Integer book = iterator.next();
            if (uniqueBooks.contains(book)) {
                iterator.remove();
                uniqueBooks.remove(book);
            }
        }
    }

    private static void balanceBundles(List<Set<Integer>> bundles) {
        Map<Integer, List<Set<Integer>>> collect = bundles.stream()
                .collect(Collectors.groupingBy(Set::size));
        while (collect.get(5) != null && collect.get(3) != null) {
            Set<Integer> anyBundleOf5 = collect.get(5).get(0);
            Set<Integer> anyBundleOf3 = collect.get(3).get(0);

            Optional<Integer> anyBookToTransfer = anyBundleOf5.stream()
                    .filter(book -> !anyBundleOf3.contains(book))
                    .findAny();
            if (anyBookToTransfer.isPresent()) {
                anyBundleOf5.remove(anyBookToTransfer.get());
                anyBundleOf3.add(anyBookToTransfer.get());
            }

            collect = bundles.stream()
                    .collect(Collectors.groupingBy(Set::size));
        }
    }

    private static double getDiscount(int differentBooks) {
        if (differentBooks == 2) {
            return 0.95;
        } else if (differentBooks == 3) {
            return 0.90;
        } else if (differentBooks == 4) {
            return 0.80;
        } else if (differentBooks == 5) {
            return 0.75;
        }
        return 1.0;
    }
}

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class FizzBuzz {

    private static final String FIZZ = "Fizz";
    private static final String BUZZ = "Buzz";
    private static final String FIZZ_BUZZ = FIZZ + BUZZ;
    private static final int FIZZ_NUMBER = 3;
    private static final int BUZZ_NUMBER = 5;

    public static void fizzBuzz(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.println(fizzBuzzStr(i));
        }
    }

    public static String fizzBuzzStr(int n) {
        Optional<String> result = Optional.empty();
        if (n % FIZZ_NUMBER == 0) {
            result = Optional.of(FIZZ);
        }
        if (n % BUZZ_NUMBER == 0) {
            result = result.isPresent() ? result.map(fizz -> fizz + BUZZ) : Optional.of(BUZZ);
        }
        return result.orElseGet(() -> String.valueOf(n));
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        fizzBuzz(n);

        bufferedReader.close();
    }
}

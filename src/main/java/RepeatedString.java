import java.io.*;
import java.util.Scanner;

public class RepeatedString {

    // Complete the repeatedString function below.
    static long repeatedString(String s, long n) {
        if (!s.contains("a")) {
            return 0L;
        }

        long fullRepeat = n / s.length();
        long rest = n % s.length();

        long nbA = 0L;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'a') {
                nbA += fullRepeat;
                if (i < rest) {
                    nbA++;
                }
            }
        }
        return nbA;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scanner.nextLine();

        long n = scanner.nextLong();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        long result = repeatedString(s, n);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/minimum-swaps-2/problem
 */
public class MinimumSwaps {

    static int minimumSwaps(int[] array) {
        int nbSwaps = 0;
        while (!isSorted(array)) {
            swap2Elements(array);
            nbSwaps++;
        }
        return nbSwaps;
    }

    private static void swap2Elements(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int j = findIndexOfMinimum(array, i + 1);
            if (array[i] > array[j]) {
                int tmp = array[i];
                array[i] = array[j];
                array[j] = tmp;
                return;
            }
        }
    }

    private static int findIndexOfMinimum(int[] array, int currentIndex) {
        int minimumIndex = currentIndex;
        int minimum = array[currentIndex];
        for (int i = currentIndex + 1; i < array.length; i++) {
            if (array[i] < minimum) {
                minimum = array[i];
                minimumIndex = i;
            }
        }
        return minimumIndex;
    }

    private static boolean isSorted(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1])
                return false;
        }
        return true;
    }
}

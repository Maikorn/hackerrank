import java.util.*;
import java.util.stream.Collectors;

public class RecommendationSystem {

    static int getMinScore(int nbProducts, List<Integer> productsFrom, List<Integer> productsTo) {
        Graph graph = new Graph(nbProducts);
        for (int i = 0; i < productsFrom.size(); i++) {
            graph.addEdge(productsFrom.get(i), productsTo.get(i));
        }
        System.out.println("graph = " + graph);
        Set<Trio> trios = graph.findTrios();
        System.out.println("trios = " + trios);
        if (trios.isEmpty()) {
            return -1;
        } else {
            return trios.stream()
                    .mapToInt(graph::computeTrioScore)
                    .min().orElse(0);
        }
    }
}

class Trio {
    Set<Product> trioProducts;

    Trio(Product a, Product b, Product c) {
        this.trioProducts = new HashSet<>(Arrays.asList(a, b, c));
    }

    Trio(int a, int b, int c) {
        this(new Product(a), new Product(b), new Product(c));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trio trio = (Trio) o;
        return Objects.equals(trioProducts, trio.trioProducts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trioProducts);
    }

    @Override
    public String toString() {
        return "Trio" + trioProducts;
    }
}

class Graph {
    Map<Product, List<Product>> adjVertices = new HashMap<>();

    Graph(Integer nbProducts) {
        for (int i = 1; i <= nbProducts; i++) {
            addProduct(i);
        }
    }

    void addProduct(Integer productNumber) {
        adjVertices.putIfAbsent(new Product(productNumber), new ArrayList<>());
    }

    void addEdge(Integer product1, Integer product2) {
        Product p1 = new Product(product1);
        Product p2 = new Product(product2);
        adjVertices.get(p1).add(p2);
        adjVertices.get(p2).add(p1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Graph graph = (Graph) o;
        return Objects.equals(adjVertices, graph.adjVertices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(adjVertices);
    }

    @Override
    public String toString() {
        return "Graph" + adjVertices;
    }

    Set<Trio> findTrios() {
        if (adjVertices.size() < 3) {
            return Collections.emptySet();
        }
        Set<Trio> trios = new HashSet<>();
        for (Map.Entry<Product, List<Product>> entry1 : adjVertices.entrySet()) {
            Product p1 = entry1.getKey();
            List<Product> products = entry1.getValue();
            if (products.size() >= 2) {
                for (Product p2 : products) {
                    List<Product> products2 = adjVertices.get(p2);
                    Set<Product> intersection = products.stream()
                            .filter(products2::contains)
                            .filter(product -> !product.equals(p1) && !product.equals(p2))
                            .collect(Collectors.toSet());
                    for (Product p3 : intersection) {
                        trios.add(new Trio(p1, p2, p3));
                    }
                }
            }
        }
        return trios;
    }

    int computeTrioScore(Trio trio) {
        Set<Product> trioProducts = trio.trioProducts;
        int score = 0;
        for (Product trioProduct : trioProducts) {
            List<Product> products = adjVertices.get(trioProduct);
            System.out.println("score += score(" + trioProduct + ") = countExclude(" + products + ")");
            score += products.stream()
                    .filter(product -> !trioProducts.contains(product))
                    .count();
        }
        return score;
    }
}

class Product {
    Integer id;

    Product(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return id.toString();
    }
}

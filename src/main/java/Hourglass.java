import java.util.Arrays;
import java.util.stream.Collectors;

public class Hourglass {

    private static final java.lang.String WHITE_SPACE = " ";
    private static final java.lang.String LINE_SEPARATOR = System.lineSeparator();

    static int hourglassSum(int[][] array) {
        int maxHourglassSum = Integer.MIN_VALUE;
        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 5; j++) {
                int hourglassSum = array[i - 1][j - 1]
                        + array[i - 1][j]
                        + array[i - 1][j + 1]
                        + array[i][j]
                        + array[i + 1][j - 1]
                        + array[i + 1][j]
                        + array[i + 1][j + 1];
                if (hourglassSum > maxHourglassSum) {
                    maxHourglassSum = hourglassSum;
                }
            }
        }
        return maxHourglassSum;
    }

    static String printArray(int[][] array) {
        return Arrays.stream(array)
                .map(line -> Arrays.stream(line)
                        .mapToObj(String::valueOf)
                        .collect(Collectors.joining(WHITE_SPACE)))
                .collect(Collectors.joining(LINE_SEPARATOR));
    }

    static String printHourglassSums(int[][] array) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 5; j++) {
                int hourglassSum = array[i-1][j-1]
                        + array[i-1][j]
                        + array[i-1][j+1]
                        + array[i][j]
                        + array[i+1][j-1]
                        + array[i+1][j]
                        + array[i+1][j+1];
                sb.append(hourglassSum);
                if (j < 4) sb.append(WHITE_SPACE);
            }
            if (i < 4) sb.append(LINE_SEPARATOR);
        }
        return sb.toString();
    }
}

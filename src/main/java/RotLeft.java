public class RotLeft {

    //{1,2,3,4,5,6,7,8,9} leftRotations = 4
    //{5,6,7,8,9,1,2,3,4}
    static int[] rotLeft(int[] array, int leftRotations) {
        int[] result = new int[array.length];
        int pivot = array.length - leftRotations;
        if (pivot >= 0) {
            System.arraycopy(array, leftRotations, result, 0, pivot);
        }
        if (leftRotations >= 0) {
            System.arraycopy(array, 0, result, pivot, leftRotations);
        }
        return result;
    }
}

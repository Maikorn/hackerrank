import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class RotLeftTest {

    @Test
    void whenGivenArrayWithoutRotation_shouldReturnUnchangedArray() {
        int[] expected = {1, 2, 3};
        assertArrayEquals(expected, RotLeft.rotLeft(new int[]{1, 2, 3}, 0));
    }

    @Test
    void whenGivenSingleElementArrayWith1Rotation_shouldReturnArray() {
        int[] expected = {1};
        assertArrayEquals(expected, RotLeft.rotLeft(new int[]{1}, 1));
    }

    @Test
    void whenGivenArrayWith1Rotation_shouldReturnRotatedArray() {
        int[] expected = {2, 1};
        assertArrayEquals(expected, RotLeft.rotLeft(new int[]{1, 2}, 1));
    }

    @Test
    void whenGivenArrayWith4Rotation_shouldReturnRotatedArray() {
        int[] expected = {5, 6, 7, 8, 9, 1, 2, 3, 4};
        assertArrayEquals(expected, RotLeft.rotLeft(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, 4));
    }
}
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HourglassTest {


    private static final String LINE_SEPARATOR = System.lineSeparator();

    @Test
    void whenArrayIsAllZeros_ShouldReturnZero() {
        int[][] array = new int[6][6];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                array[i][j] = 0;
            }
        }
        assertEquals(0, Hourglass.hourglassSum(array));
    }

    @Test
    void printArray() {
        String expected =
                "0 0 0 0 0 0" + LINE_SEPARATOR +
                        "1 1 1 1 1 1" + LINE_SEPARATOR +
                        "2 2 2 2 2 2" + LINE_SEPARATOR +
                        "3 3 3 3 3 3" + LINE_SEPARATOR +
                        "4 4 4 4 4 4" + LINE_SEPARATOR +
                        "5 5 5 5 5 5";

        int[][] array = new int[][]{
                {0, 0, 0, 0, 0, 0},
                {1, 1, 1, 1, 1, 1},
                {2, 2, 2, 2, 2, 2},
                {3, 3, 3, 3, 3, 3},
                {4, 4, 4, 4, 4, 4},
                {5, 5, 5, 5, 5, 5}
        };

        assertEquals(expected, Hourglass.printArray(array));
    }

    @Test
    void printHourglasses() {
        String expected =
                "-63 -34 -9 12" + LINE_SEPARATOR +
                "-10 0 28 23" + LINE_SEPARATOR +
                "-27 -11 -2 10" + LINE_SEPARATOR +
                "9 17 25 18";

        int[][] array = new int[][]{
                {-9, -9, -9, 1, 1, 1},
                {0, -9, 0, 4, 3, 2},
                {-9, -9, -9, 1, 2, 3},
                {0, 0, 8, 6, 6, 0},
                {0, 0, 0, -2, 0, 0},
                {0, 0, 1, 2, 4, 0}
        };

        assertEquals(expected, Hourglass.printHourglassSums(array));
    }

    @Test
    void findMaxHourglassSum() {
        int[][] array = new int[][]{
                {-9, -9, -9, 1, 1, 1},
                {0, -9, 0, 4, 3, 2},
                {-9, -9, -9, 1, 2, 3},
                {0, 0, 8, 6, 6, 0},
                {0, 0, 0, -2, 0, 0},
                {0, 0, 1, 2, 4, 0}
        };

        assertEquals(28, Hourglass.hourglassSum(array));
    }

}
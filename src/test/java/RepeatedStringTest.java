import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RepeatedStringTest {

    @Test
    void whenRepeatingNotAOnce_shouldReturn0() {
        assertEquals(0, RepeatedString.repeatedString("b", 1));
    }

    @Test
    void whenRepeatingNotA1000times_shouldReturn0() {
        assertEquals(0, RepeatedString.repeatedString("b", 1000));
    }

    @Test
    void whenRepeatingAOnce_shouldReturn1() {
        assertEquals(1, RepeatedString.repeatedString("a", 1));
    }

    @Test
    void whenRepeatingA1000times_shouldReturn1() {
        assertEquals(1000, RepeatedString.repeatedString("a", 1000));
    }

    @Test
    void whenRepeatingABAUntil10Chars_shouldReturn7() {
        assertEquals(7, RepeatedString.repeatedString("aba", 10));
    }

    @Test
    void whenRepeatingABAUntil10000000Chars_shouldReturn7() {
        assertEquals(1000000, RepeatedString.repeatedString("aaa", 1000000));
    }

    @Test
    void whenRepeatingABCAC_10Chars_shouldReturn4() {
        assertEquals(4, RepeatedString.repeatedString("abcac", 10));
    }

    @Test
    void whenRepeatingLongString_CutAt10Chars_shouldReturn5() {
        assertEquals(5, RepeatedString.repeatedString("ababababababababababababababababababababab", 10));
    }

}
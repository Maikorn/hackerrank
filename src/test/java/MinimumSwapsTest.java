import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MinimumSwapsTest {

    @Test
    void minimumSwapsOfSortedList_shouldBe0() {
        assertEquals(0, MinimumSwaps.minimumSwaps(new int[]{1, 2}));
        assertEquals(0, MinimumSwaps.minimumSwaps(new int[]{1, 2, 3}));
        assertEquals(0, MinimumSwaps.minimumSwaps(new int[]{2, 3, 4}));
    }

    @Test
    void minimumSwapsOf2UnsortedElements_shouldbe1() {
        assertEquals(1, MinimumSwaps.minimumSwaps(new int[]{2, 1}));
        assertEquals(1, MinimumSwaps.minimumSwaps(new int[]{4, 3}));
    }

    @Test
    void minimumSwapsOf3UnsortedElements() {
        assertEquals(1, MinimumSwaps.minimumSwaps(new int[]{1, 3, 2})); // swap 3<>2
        assertEquals(1, MinimumSwaps.minimumSwaps(new int[]{2, 1, 3})); // swap 2<>1
        assertEquals(1, MinimumSwaps.minimumSwaps(new int[]{3, 2, 1})); // swap 3<>1
        assertEquals(2, MinimumSwaps.minimumSwaps(new int[]{3, 1, 2})); // swap 3<>2 + 2<>1
    }

    @Test
    void givenExamples() {
        assertEquals(5, MinimumSwaps.minimumSwaps(new int[]{7, 1, 3, 2, 4, 5, 6}));
        assertEquals(3, MinimumSwaps.minimumSwaps(new int[]{4, 3, 1, 2}));
    }
    
    @Test
    void hugeExample() {
    }
}

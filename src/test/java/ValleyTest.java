import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValleyTest {

    @Test
    void whenPathIsEmpty_shouldReturn0() {
        assertEquals(0, Valley.countingValleys(0, ""));
    }

    @Test
    void whenPathIsSimpleMountain_shouldReturn0() {
        assertEquals(0, Valley.countingValleys(2, "UD"));
    }
    
    @Test
    void whenPathIsSimpleValley_shouldReturn1() {
        assertEquals(1, Valley.countingValleys(2, "DU"));
    }

    @Test
    void whenPathIsValleyThenMountain_shouldReturn1() {
        assertEquals(1, Valley.countingValleys(4, "DUUD"));
    }

    @Test
    void whenPathIsValleyMountainValley_shouldReturn2() {
        assertEquals(2, Valley.countingValleys(6,"DUUDDU"));
    }

    @Test
    void fullExample() {
        assertEquals(1, Valley.countingValleys(8, "UDDDUDUU"));
    }

}
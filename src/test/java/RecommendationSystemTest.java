import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RecommendationSystemTest {

    @Test
    void testSimplestGraphs() {
        assertEquals(-1, RecommendationSystem.getMinScore(2, list(1), list(2)));
        assertEquals(-1, RecommendationSystem.getMinScore(3, list(1, 1), list(2, 3)));
        assertEquals(-1, RecommendationSystem.getMinScore(3, list(1, 2), list(2, 3)));
    }

    @Test
    void testSimplestTrioGraph() {
        Graph graph = new Graph(3);
        graph.addEdge(1, 2);
        graph.addEdge(1, 3);
        graph.addEdge(2, 3);

        Trio trio = new Trio(1, 2, 3);
        Set<Trio> expected = Collections.singleton(trio);
        assertEquals(1, graph.findTrios().size());
        assertEquals(expected, graph.findTrios());

        assertEquals(0, graph.computeTrioScore(trio));

        assertEquals(0, RecommendationSystem.getMinScore(3, list(1, 1, 2), list(2, 3, 3)));
    }

    @Test
    void testDoubleTrioGraph() {
        Graph graph = new Graph(6);
        graph.addEdge(1, 2);
        graph.addEdge(1, 3);
        graph.addEdge(2, 3);
        graph.addEdge(4, 5);
        graph.addEdge(4, 6);
        graph.addEdge(5, 6);

        Trio trio123 = new Trio(1, 2, 3);
        Trio trio456 = new Trio(4, 5, 6);
        Set<Trio> expected = new HashSet<>(Arrays.asList(trio123, trio456));
        assertEquals(expected.size(), graph.findTrios().size());
        assertEquals(expected, graph.findTrios());

        assertEquals(0, graph.computeTrioScore(trio123));
        assertEquals(0, graph.computeTrioScore(trio456));

        assertEquals(0, RecommendationSystem.getMinScore(6, list(1, 1, 2, 4, 4, 5), list(2, 3, 3, 5, 6, 6)));
    }

    @Test
    void testDoubleTrioGraphWithOneEdgeBetweenThem() {
        Graph graph = new Graph(6);
        graph.addEdge(1, 2);
        graph.addEdge(1, 3);
        graph.addEdge(2, 3);
        graph.addEdge(4, 5);
        graph.addEdge(4, 6);
        graph.addEdge(5, 6);
        graph.addEdge(3, 4);

        Trio trio123 = new Trio(1, 2, 3);
        Trio trio456 = new Trio(4, 5, 6);
        Set<Trio> expected = new HashSet<>(Arrays.asList(trio123, trio456));
        assertEquals(expected.size(), graph.findTrios().size());
        assertEquals(expected, graph.findTrios());

        assertEquals(1, graph.computeTrioScore(trio123));
        assertEquals(1, graph.computeTrioScore(trio456));

        assertEquals(1, RecommendationSystem.getMinScore(6, list(1, 1, 2, 4, 4, 5, 3), list(2, 3, 3, 5, 6, 6, 4)));
    }

    @Test
    void testTrioGraphWithRealScore() {
        Graph graph = new Graph(6);
        graph.addEdge(1, 2);
        graph.addEdge(2, 3);
        graph.addEdge(2, 5);
        graph.addEdge(3, 5);
        graph.addEdge(4, 5);
        graph.addEdge(5, 6);

        Trio trio235 = new Trio(2,3,5);
        Set<Trio> expected = new HashSet<>(Arrays.asList(trio235));
        assertEquals(expected.size(), graph.findTrios().size());
        assertEquals(expected, graph.findTrios());

        assertEquals(3, graph.computeTrioScore(trio235));

        assertEquals(3, RecommendationSystem.getMinScore(6, list(1, 2, 2, 3, 4, 5), list(2, 3, 5, 5, 5, 6)));
    }


    private List<Integer> list(Integer... ints) {
        return Arrays.asList(ints);
    }

}
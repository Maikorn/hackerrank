import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SockMerchantTest {

    @Test
    void whenArrayIsEmpty_shouldReturn0() {
        assertEquals(0, SockMerchant.sockMerchant(0, new int[]{}));
    }

    @Test
    void whenArrayContainsExactlyTwiceTheSameNumber_shouldReturn1() {
        assertEquals(1, SockMerchant.sockMerchant(2, new int[]{1, 1}));
        assertEquals(1, SockMerchant.sockMerchant(2, new int[]{2, 2}));
    }

    @Test
    void whenArrayContainsThriceTheSameNumber_shouldReturn1() {
        assertEquals(1, SockMerchant.sockMerchant(3, new int[]{1, 1, 1}));
        assertEquals(1, SockMerchant.sockMerchant(3, new int[]{2, 2, 2}));
    }

    @Test
    void whenArrayContains2Pairs_shouldReturn2() {
        assertEquals(2, SockMerchant.sockMerchant(4, new int[]{1, 1, 1, 1}));
        assertEquals(2, SockMerchant.sockMerchant(4, new int[]{1, 1, 2, 2}));
        assertEquals(2, SockMerchant.sockMerchant(4, new int[]{1, 2, 1, 2}));
    }

    @Test
    void fullExample() {
        assertEquals(3, SockMerchant.sockMerchant(9, new int[]{10, 20, 20, 10, 10, 30, 50, 10, 20}));
    }
}
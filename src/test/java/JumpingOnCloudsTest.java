import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JumpingOnCloudsTest {

    @Test
    void when2SafeClouds_shouldReturn1() {
        assertEquals(1, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0}));
    }
    
    @Test
    void when3SafeClouds_shouldSkip1AndReturn1() {
        assertEquals(1, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0}));
    }

    @Test
    void when3CloudsWith1Thunder_shouldSkipThunderAndReturn1() {
        assertEquals(1, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0}));
    }

    @Test
    void when4Clouds_shouldReturn2() {
        assertEquals(2, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0, 0}));
        assertEquals(2, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 1, 0}));
        assertEquals(2, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0, 0}));
    }

    @Test
    void when5Clouds_shouldReturn3() {
        assertEquals(2, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0, 0, 0}));
        assertEquals(2, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0, 0, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 1, 0, 0}));
        assertEquals(2, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0, 1, 0}));
        assertEquals(2, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0, 1, 0}));
    }

    @Test
    void when6Clouds_shouldReturn3() {
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0, 0, 0, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0, 0, 0, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 1, 0, 0, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0, 1, 0, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0, 0, 1, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 1, 0, 1, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0, 1, 0, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0, 0, 1, 0}));
    }

    @Test
    void when7Clouds_shouldReturn3() {
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0, 0, 0, 0, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0, 0, 0, 0, 0}));
        assertEquals(4, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 1, 0, 0, 0, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0, 1, 0, 0, 0}));
        assertEquals(4, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0, 0, 1, 0, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0, 0, 0, 1, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0, 1, 0, 0, 0}));
        assertEquals(4, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0, 0, 1, 0, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0, 0, 0, 1, 0}));
        assertEquals(4, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 1, 0, 1, 0, 0}));
        assertEquals(4, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 1, 0, 0, 1, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 0, 0, 1, 0, 1, 0}));
        assertEquals(3, JumpingOnClouds.jumpingOnClouds(new int[]{0, 1, 0, 1, 0, 1, 0}));
    }

}
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class HarryPotterTest {

    @Test
    void whenBuying0Book_shouldReturn0() {
        double price = HarryPotter.getPrice(Collections.emptyList());

        assertEquals(0.0, price, 0.0);
    }

    @Test
    void whenBuyingAnyBook_shouldReturn8() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1)));

        assertEquals(8.0, price, 0.0);
    }

    @Test
    void whenBuyingAnyBookTwice_shouldReturn16() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1, 1)));

        double expectedPrice = 2 * 8.0;
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuying2DifferentBooks_shouldReturn16Minus5Percent() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1, 2)));

        double expectedPrice = 2 * 8.0 * 0.95;
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuying3DifferentBooks_shouldReturn24Minus10Percent() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1, 2, 3)));

        double expectedPrice = 3 * 8.0 * 0.90;
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuyingAnyBookThrice_shouldReturn24() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1, 1, 1)));

        double expectedPrice = 3 * 8.0;
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuying4DifferentBooks_shouldReturn32Minus20Percent() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));

        double expectedPrice = 4 * 8.0 * 0.80;
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuying5DifferentBooks_shouldReturn40Minus25Percent() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5)));

        double expectedPrice = 5 * 8.0 * 0.75;
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuying112Books_shouldReturn16Minus5PercentPlus8() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1, 1, 2)));

        double expectedPrice = 2 * 8.0 * 0.95 + 8.0;
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuying1212Books_shouldReturn2Times16Minus5Percent() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1, 2, 1, 2)));

        double expectedPrice = 2 * (2 * 8.0 * 0.95);
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuying121212Books_shouldReturn3Times16Minus5Percent() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1, 2, 1, 2, 1, 2)));

        double expectedPrice = 3 * (2 * 8.0 * 0.95);
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuying12341231Books_shouldReturn55_2() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1, 2, 3, 1, 2, 3, 4, 1)));

        double expectedPrice = (4 * 8.0 * 0.80) + (3 * 8.0 * 0.90) + (1 * 8.0 * 1.0);
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuying12341235Books_shouldGroupIntoGroupsOf4() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(1, 2, 3, 4, 1, 2, 3, 5)));

        double expectedPrice = (4 * 8.0 * 0.80) + (4 * 8.0 * 0.80);
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuying3BundlesOf5And2BundlesOf4Books_shouldReturnGoodPrice() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(
                1,1,1,1,1,
                2,2,2,2,2,
                3,3,3,3,
                4,4,4,4,4,
                5,5,5,5
        )));

        double expectedPrice = 3 * (8 * 5 * 0.75) + 2 * (8 * 4 * 0.8);
        assertEquals(expectedPrice, price, 0.0);
    }

    @Test
    void whenBuying2BundlesOf5And2BundlesOf3Books_shouldReturnBestPrice() {
        double price = HarryPotter.getPrice(new ArrayList<>(Arrays.asList(
                1,2,3,4,5,
                1,2,3,4,5,
                1,2,3,
                1,2,3
        )));

        double expectedPrice = 4 * (8 * 4 * 0.8);
        assertEquals(expectedPrice, price, 0.0);
    }

}